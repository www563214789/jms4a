from flask import Flask, Response, request
import xmltodict

'''
這裡要import你的辨識程式的function
import L4A_MDI_Yolov3
'''

app = Flask(__name__)

@app.route('/MDIAIWebService', methods=['GET', 'POST'])
def MDIAIWebService():

    input_data = request.data # 取得HTTP Request內容
    input_data_dict = xmltodict.parse(input_data) # 將XML的內容轉成Dict的格式處理

    trx_id = input_data_dict['transaction']['trx_id'] # 取得trx_id的字串來判斷是哪個事件
    output_data = trx_id(input_data_dict)
    output_data_dict = xmltodict.unparse(output_data)

    return Response(output_data_dict, mimetype='text/xml')

# Query 此M/N是否支援AI功能
def MAINQMNS(data):
    
    data['transaction']['type_id'] = 'O'
    data['transaction']['rtn_code'] = '0000000'
    data['transaction']['ai_flag'] = 'Y'
    data['transaction']['ai_learning'] = '95'

    return data

# Query 此Glass經AI判片OX
def MAINQCMA(data):

    '''
    # 這裡要去呼叫你的判斷程式
    # 只是資料夾有分左右
    # 有個前提要弄清楚 不管資料夾會有幾張照片 只會回傳一個值

    chip_one_hot = L4A_MDI_Yolov3()
    '''

    data['transaction']['type_id'] = 'O'
    data['transaction']['rtn_code'] = '0000000'
    # data['transaction']['ai_pnl_grade'] = '000100000000000100000000'
    data['transaction']['ai_pnl_grade'] = chip_one_hot

    return data

# 儲存MDI OX資訊
def MARPNLGD(data):
    pass


if __name__ == "__main__":
    app.run('0.0.0.0', port=8080)